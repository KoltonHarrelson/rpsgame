package hsutx.koltonharrelson.rpsgame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class ComputerMatch extends AppCompatActivity {

    /*
     * This section declares the variables that will be used throughout the entirety
     * of the program.
     */

    Button rock, scissors, paper, reset; //These buttons allow me to set up the needed event listeners
    ImageView playerChoice, cpuChoice;
    TextView matchResult, winsLosses;
    int playerPick, computerPick; //These pertain to the integer values associated with RPS choices
    String [][] outcomeArray = new String[3][3];
    int wins, losses;
    FileOutputStream fileOut;
    FileInputStream fileIn;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_match);
        setOutcomeArray();
        initializeScores();
        file=new File(getFilesDir()+"/scores.csv");


        paper = (Button) findViewById(R.id.paper);
        scissors = (Button) findViewById(R.id.scissors);
        rock = (Button) findViewById(R.id.rock);
        reset = (Button) findViewById(R.id.resetButton);

        playerChoice = (ImageView) findViewById(R.id.playerChoice);
        cpuChoice = (ImageView) findViewById(R.id.computerChoice);

        matchResult = (TextView) findViewById(R.id.matchResult);
        winsLosses = (TextView) findViewById(R.id.scoreTracker);
        try {
            loadScores();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*
         * The following listeners will track the clicks by the user on the buttons associated with
         * the appropriate symbols for the game. When one is clicked, the ImageView playerChoice
         * will be set to the matching symbol.
         */

        paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerPick = 2;
                playerChoice.setImageResource(R.drawable.paper);
                rpsMatch(playerPick);
            }
        });

        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerPick = 1;
                playerChoice.setImageResource(R.drawable.rock);
                rpsMatch(playerPick);
            }
        });

        scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerPick = 3;
                playerChoice.setImageResource(R.drawable.scissors);
                rpsMatch(playerPick);
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetScores();
            }
        });
    }
    /*
     * The following method plays the match of rock paper scissors between the human player and the
     * computer player. It accepts a parameter playerPick as an integer, with 1 being rock, 2 being
     * paper, and 3 being scissors.
     */

    public void rpsMatch(int playerPick){
        Random pick= new Random();
        computerPick=pick.nextInt(3)+1; //This random choice selects a random from 1 to 3
        if (computerPick==1){ cpuChoice.setImageResource(R.drawable.rock);}
        else if (computerPick==2){ cpuChoice.setImageResource(R.drawable.paper);}
        else{ cpuChoice.setImageResource(R.drawable.scissors);}

        String outcome=outcomeArray[playerPick-1][computerPick-1];

        matchResult.setText(outcomeArray[playerPick-1][computerPick-1]);
        if(outcome.equals(getString(R.string.computer_win))){
            losses ++;
        }
        else if(outcome.equals(getString(R.string.player_win))){
            wins ++;
        }
        String scores= wins + "-" + losses;
        winsLosses.setText(scores);
    }

    public void mainMenu(View view) throws IOException {
        writeScores();
        finish();
    }

    public void loadScores() throws IOException, FileNotFoundException{
        fileIn = null;
        fileIn = openFileInput("scores.csv");
        InputStreamReader streamReader = new InputStreamReader(fileIn);
        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder builder = new StringBuilder();
        String score;
        score = br.readLine();
        builder.append(score);
        String scoreHolder = builder.toString();
        wins = Integer.parseInt(scoreHolder);
        builder.setLength(0);
        score = br.readLine();
        builder.append(score);
        scoreHolder = builder.toString();
        losses = Integer.parseInt(scoreHolder);
        winsLosses.setText(wins + "-" + losses);
    }

    public void writeScores() throws IOException{
        file.delete();
        String writeWins = Integer.toString(wins);
        String writeLosses = Integer.toString(losses);
        fileOut = null;
        fileOut = openFileOutput("scores.csv", MODE_APPEND);
        fileOut.write(writeWins.getBytes());
        fileOut.write(10);
        fileOut.write(writeLosses.getBytes());
        fileOut.close();
    }

    public void resetScores(){
        file.delete();
        wins = losses = 0;
        winsLosses.setText("0-0");
    }

    public void initializeScores(){
        wins = losses = 0;
    }

    public void setOutcomeArray(){
        outcomeArray[0][0] = getString(R.string.draw);
        outcomeArray[0][1] = getString(R.string.computer_win);
        outcomeArray[0][2] = getString(R.string.player_win);
        outcomeArray[1][0] = getString(R.string.player_win);
        outcomeArray[1][1] = getString(R.string.draw);
        outcomeArray[1][2] = getString(R.string.computer_win);
        outcomeArray[2][0] = getString(R.string.computer_win);
        outcomeArray[2][1] = getString(R.string.player_win);
        outcomeArray[2][2] = getString(R.string.draw);
    }
}