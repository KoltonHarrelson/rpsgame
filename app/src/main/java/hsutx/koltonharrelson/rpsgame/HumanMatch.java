package hsutx.koltonharrelson.rpsgame;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class HumanMatch extends AppCompatActivity {

    Button rock, scissors, paper, reset; //These buttons allow me to set up the needed event listeners
    ImageView playerOneChoice, playerTwoChoice;
    TextView matchResult, winsLosses;
    ArrayList<Integer> pickList = new ArrayList<>();
    int playerOnePick, playerTwoPick, pick; //These pertain to the integer values associated with RPS choices
    String [][] outcomeArray;
    int wins, losses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_match);
        setOutcomeArray();
        initializeScores();

        paper = (Button) findViewById(R.id.paper2);
        scissors = (Button) findViewById(R.id.scissors2);
        rock = (Button) findViewById(R.id.rock2);
        reset = (Button) findViewById(R.id.resetButton2);

        playerOneChoice = (ImageView) findViewById(R.id.playerOneChoice);
        playerTwoChoice = (ImageView) findViewById(R.id.playerTwoChoice);

        matchResult = (TextView) findViewById(R.id.matchResult2);
        winsLosses = (TextView) findViewById(R.id.scoreTracker2);


        paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pick = 2;
                pickList.add(pick);
                if(pickList.size()==2){
                    rpsMatch(pickList.get(0), pickList.get(1));
                }
            }
        });

        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pick = 1;
                pickList.add(pick);
                if(pickList.size()==2){
                    rpsMatch(pickList.get(0), pickList.get(1));
                }
            }
        });

        scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pick = 3;
                pickList.add(pick);
                if(pickList.size()==2){
                    rpsMatch(pickList.get(0), pickList.get(1));
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetScores();
            }
        });


    }
    /*
     * The following method plays the match of rock paper scissors between the human player and the
     * computer player. It accepts a parameter playerPick as an integer, with 1 being rock, 2 being
     * paper, and 3 being scissors.
     */

    public void rpsMatch(int one, int two){

        matchResult.setText(outcomeArray[one-1][two-1]);
        String outcome = outcomeArray[one-1][two-1];

        if (outcome.equals(getString(R.string.player_one_win))){
            wins++;
        }
        else if(outcome.equals(getString(R.string.player_two_win))){
            losses++;
        }
        String scores = wins + "-" + losses;
        winsLosses.setText(scores);
        pickList.removeAll(pickList);

    }




    public void mainMenu(View view){
        finish();
    }

    public void resetScores(){
        wins = losses = 0;
        winsLosses.setText("0-0");
    }


    public void initializeScores(){
        wins = losses = 0;
    }

    public void setOutcomeArray(){
        outcomeArray = new String[3][3];
        outcomeArray[0][0] = getString(R.string.draw);
        outcomeArray[0][1] = getString(R.string.player_two_win);
        outcomeArray[0][2] = getString(R.string.player_one_win);
        outcomeArray[1][0] = getString(R.string.player_one_win);
        outcomeArray[1][1] = getString(R.string.draw);
        outcomeArray[1][2] = getString(R.string.player_two_win);
        outcomeArray[2][0] = getString(R.string.player_two_win);
        outcomeArray[2][1] = getString(R.string.player_one_win);
        outcomeArray[2][2] = getString(R.string.draw);
    }
}