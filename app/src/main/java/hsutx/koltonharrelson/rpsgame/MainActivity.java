/*
 * @author Kolton Harrelson
 * @version April 5, 2022
 * @date April 5, 2022
 *
 * The following program serves to play a match of rock paper scissors between a human and a
 * computer controlled opponent. This is the first proof of concept/prototype which will develop
 * into one part of the overall final app goal.
 */
package hsutx.koltonharrelson.rpsgame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.*;

import android.os.Bundle;

import java.util.Random;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void humanMatch(View view){
        Intent startHumanMatch = new Intent(this, HumanMatch.class);
        startActivity(startHumanMatch);
    }

    public void cpuMatch(View view){
        Intent startCPUMatch = new Intent(this, ComputerMatch.class);
        startActivity(startCPUMatch);
    }



}